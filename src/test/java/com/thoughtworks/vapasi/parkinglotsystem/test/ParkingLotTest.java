package com.thoughtworks.vapasi.parkinglotsystem.test;

import com.thoughtworks.vapasi.parkinglotsystem.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {

    @Test
    public void shouldAllocateParkingSlot() {
        //Arrange
        Vehicle vehicle = new Vehicle("REGNO");
        ParkingLot parkingLot = new ParkingLot(500);

        //Act
        ParkingToken token = parkingLot.allocateSlot(vehicle);

        //Assert
        assertNotNull(token);
        assertEquals(vehicle.getVehicleNo(), token.getVehicleNo());
    }

    @Test
    public void shouldDeAllocateParkingSlotForAValidToken() throws InvalidTokenException {
        //Arrange
        Vehicle vehicle = new Vehicle("REGNO");
        ParkingLot parkingLot = new ParkingLot(500);

        //Act
        ParkingToken token = parkingLot.allocateSlot(vehicle);
        Vehicle removedVehicle = parkingLot.removeSlot(token);

        //Assert
        assertEquals(vehicle.getVehicleNo(),removedVehicle.getVehicleNo());
        assertEquals(token.getVehicleNo(),removedVehicle.getVehicleNo());
    }

    @Test
    public void shouldNotDeAllocateParkingSlotForAInValidToken() {
        //Arrange
        Vehicle vehicle = new Vehicle("REGNO");
        ParkingLot parkingLot = new ParkingLot(500);
        ParkingToken invalidToken = new ParkingToken("REGNo002");

        //Act
        ParkingToken token = parkingLot.allocateSlot(vehicle);

        //Assert
        assertThrows(InvalidTokenException.class,() -> {parkingLot.removeSlot(invalidToken);});
    }

}
