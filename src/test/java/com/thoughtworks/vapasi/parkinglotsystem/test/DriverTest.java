package com.thoughtworks.vapasi.parkinglotsystem.test;

import com.thoughtworks.vapasi.parkinglotsystem.Driver;
import com.thoughtworks.vapasi.parkinglotsystem.ParkingLot;
import com.thoughtworks.vapasi.parkinglotsystem.ParkingToken;
import com.thoughtworks.vapasi.parkinglotsystem.Vehicle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DriverTest {

    @Test
    public void shouldAllocateParkingSlot() {
        //Arrange
        Vehicle vehicle = new Vehicle("REGNO");
        Driver driver = new Driver("driver1", vehicle);
        ParkingLot parkingLot = new ParkingLot(500);

        //Act
        ParkingToken token = driver.parkVehicle(parkingLot);

        //Assert
        assertNotNull(token);
        assertEquals(vehicle.getVehicleNo(), token.getVehicleNo());
    }

    @Test
    public void shouldUnParkTheVehicle(){

        Vehicle vehicle=new Vehicle("REGNO");
        Driver driver = new Driver("driver",vehicle);
        ParkingLot parkingLot= new ParkingLot(500);

        driver.parkVehicle(parkingLot);

        Vehicle unParkedVehicle = driver.unParkVehicle(parkingLot);
        assertEquals(vehicle,unParkedVehicle);

        assertFalse(driver.hasParkingToken());

    }

}
