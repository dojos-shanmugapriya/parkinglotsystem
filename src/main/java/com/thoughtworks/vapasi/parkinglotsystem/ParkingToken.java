package com.thoughtworks.vapasi.parkinglotsystem;

public class ParkingToken {
    private String vehicleNo;


    public ParkingToken(String  vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }
}
