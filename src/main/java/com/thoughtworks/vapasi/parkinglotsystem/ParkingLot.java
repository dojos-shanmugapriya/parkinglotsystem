package com.thoughtworks.vapasi.parkinglotsystem;


public class ParkingLot {

    private Vehicle vehicle;

    private int capacity ;


    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public ParkingToken allocateSlot(Vehicle vehicle) {
        ParkingToken token =  new ParkingToken(vehicle.getVehicleNo());
        this.vehicle=vehicle;
        return token;
    }

    public Vehicle removeSlot(ParkingToken parkingToken) throws InvalidTokenException {
        if(!(vehicle.getVehicleNo().equals(parkingToken.getVehicleNo()))){
            throw new InvalidTokenException("Invalid token provided");

        }
        return vehicle;

    }
}
